from django.db              import connection
from django.http            import JsonResponse
from django.utils.dateparse import parse_date
from django.views           import generic
from datetime               import date
from rest_framework         import viewsets

from models      import Applicants
from serializers import ApplicantSerializer

class ApplicantView(generic.TemplateView):
    template_name = 'shoppers/index.html'

class ApplicantViewSet(viewsets.ModelViewSet):
    serializer_class = ApplicantSerializer

    def get_queryset(self):
        # Don't show all applicants due to no authentication
        # Otherwise, anyone could see entire database
        queryset = Applicants.objects.none()
        applicant_email = self.request.query_params.get('email', None)
        if applicant_email is not None:
            queryset = Applicants.objects.all().order_by('-updated_at')
            queryset = queryset.filter(email=applicant_email)
        return queryset

def funnel(request):
    data   = {}
    errors = {}

    # Return an appropriate error message if any of the query parameters are not correct
    start_date = request.GET.get('start_date')
    if start_date != None:
        try:
            start_date = parse_date(start_date)
            if start_date == None:
                errors['start_date'] = "Could not parse date (use format YYYY-MM-DD)"
        except ValueError as err:
            errors['start_date'] = str(err)
    else:
        errors['start_date'] = "Query parameter is missing"

    end_date = request.GET.get('end_date')
    if end_date != None:
        try:
            end_date = parse_date(end_date)
            if end_date == None:
                errors['end_date'] = "Could not parse date (use format YYYY-MM-DD)"
        except ValueError as err:
            print err
            errors['end_date'] = str(err)

    # If the user doesn't enter an end date, use the current date as the end date
    if end_date == None:
        end_date = date.today()

    # We will return a JSON object with the error messages if the dates aren't valid
    if not errors and start_date != None and end_date != None:
        cursor = connection.cursor()

        data_query = cursor.execute(
            """
            SELECT
              workflow_state, 
              COUNT(workflow_state) AS count, 
              MAX(date(created_at, 'weekday 1')) AS week_start, 
              MAX(date(created_at, 'weekday 1', '+6 day')) AS week_end 
            FROM applicants 
            WHERE created_at 
            BETWEEN %s AND %s 
            GROUP BY 
              strftime('%%W', created_at), 
              workflow_state
            ORDER BY 
              week_start, 
              workflow_state
            """,
            (start_date, end_date)
        )

        for row in cursor.fetchall():
            workflow_state = row[0]
            count          = row[1]
            start_date     = row[2]
            end_date       = row[3]

            index = start_date + '-' + end_date
            if index not in data:
                data[index] = {}
            data[index][workflow_state] = count
    else:
        # Output errors
        data['start_date'] = start_date
        data['end_date']   = end_date
        data['errors']     = errors

    return JsonResponse(data)

