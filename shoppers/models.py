from __future__ import unicode_literals

from django.db import models
from django.core.validators import validate_email

# This is an auto-generated Django model module.
# Some settings have been added to aid server-side validation
class Applicants(models.Model):
    id             = models.AutoField(primary_key=True)
    first_name     = models.TextField()
    last_name      = models.TextField()

    REGION_CHOICES = (
        ('Berlin',                 'Berlin'),
        ('Boston',                 'Boston'),
        ('Chicago',                'Chicago'),
        ('Delhi',                  'Delhi'),
        ('NYC',                    'NYC'),
        ('San Francisco Bay Area', 'San Francisco Bay Area'),
        ('Toronto',                'Toronto'),
    )
    region = models.TextField(choices=REGION_CHOICES)

    phone  = models.TextField()

    email  = models.TextField(validators=[validate_email], unique=True)

    PHONE_TYPE_CHOICES = (
        ('Android 4.0+ (less than 2 years old)', 'Android 4.0+ (less than 2 years old)'),
        ('Android 2.2/2.3 (over 2 years old)',   'Android 2.2/2.3 (over 2 years old)'),
        ('Blackberry',                           'Blackberry'),
        ('iPhone 6/6 Plus',                      'iPhone 6/6 Plus'),
        ('iPhone 6s/6s Plus',                    'iPhone 6s/6s Plus'),
        ('iPhone 5/5S',                          'iPhone 5/5S'),
        ('iPhone 4/4S',                          'iPhone 4/4S'),
        ('iPhone 3G/3GS',                        'iPhone 3G/3GS'),
        ('Windows Phone',                        'Windows Phone'),
        ('Other',                                'Other')
    )
    phone_type     = models.TextField(choices=PHONE_TYPE_CHOICES)

    source         = models.TextField(blank=True, null=True)
    over_21        = models.NullBooleanField()
    reason         = models.TextField(blank=True, null=True)

    WORKFLOW_STATE_CHOICES = (
        ('applied',              'applied'),
        ('quiz_started',         'quiz_started'),
        ('quiz_completed',       'quiz_completed'),
        ('hired',                'hired'),
        ('onboarding_requested', 'onboarding_requested'),
        ('onboarding_completed', 'onboarding_completed')
    )
    workflow_state = models.TextField(
                       choices=WORKFLOW_STATE_CHOICES,
                       default='applied')

    created_at     = models.DateTimeField(auto_now_add=True)
    updated_at     = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed  = False
        db_table = 'applicants'
