from models import Applicants
from rest_framework import serializers

class ApplicantSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Applicants
        first_name = serializers.SerializerMethodField(source='firstName')
        firstName = serializers.CharField(source='first_name')
        fields = (
            'id',
        	'first_name',
        	'last_name',
        	'region',
        	'phone',
        	'email',
        	'phone_type',
        	'source',
        	'over_21',
        	'reason',
        	'workflow_state',
        	'created_at',
        	'updated_at'
        )
