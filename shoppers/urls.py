from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^', views.ApplicantView.as_view(), name='applicants')
]

