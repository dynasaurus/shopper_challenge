import React, { Component, PropTypes } from 'react'
import browserHistory                  from 'react-router/lib/browserHistory'

export default class LandingPage extends Component {
	render() {
		return (
			<div id="apply">
				<h2>Become an Instacart Shopper</h2>
				<button onClick={() => browserHistory.push('/shoppers/apply')}>Apply Now!</button>
			</div>
		)
	}
}
