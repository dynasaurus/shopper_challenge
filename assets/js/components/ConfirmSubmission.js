import React, { Component }  from 'react'
import { connect }           from 'react-redux'
import browserHistory        from 'react-router/lib/browserHistory'
import { submitApplication } from '../actions/data'

class ConfirmSubmission extends Component {
	render() {
		const { dispatch, values, errors } = this.props
		const { firstName, lastName, region, email, phone, phoneType, over21 } = values
		return (
			<div>
				<p>Instacart will perform a background check with the information you provided. Are you sure you want to submit your application?</p>
				<div className="form-data">
					<ul>
						<li><span>First Name</span>   {firstName}</li>
						<li><span>Last Name</span>    {lastName}</li>
						<li><span>Region</span>       {region}</li>
						<li><span>Email</span>        {email}</li>
						<li><span>Phone Number</span> {phone}</li>
						<li><span>Phone Type</span>   {phoneType}</li>
						<li><span>Over 21?</span>     {over21 ? 'Yes' : 'No'}</li>
					</ul>
				</div>
				{/* Note: Since the form was validated on the client in the previous form,       */}
				{/* the user *should* never see this message, but it's good to have just in case */}
				{errors && <div className="submit-error">
					Sorry, there were errors submitting your application.
					Please check your submission for errors and try again later.
				</div>}
				<div>
					<button onClick={() => dispatch(submitApplication(values))}>Yes, I'm sure!</button>{' '}
					<button onClick={() => browserHistory.goBack()}>No, take me back!</button>
				</div>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		values: state.data,
		errors: state.errors
	}
}

export default connect(mapStateToProps)(ConfirmSubmission)
