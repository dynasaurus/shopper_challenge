import React, { Component }  from 'react'
import { reduxForm }         from 'redux-form'
import browserHistory        from 'react-router/lib/browserHistory'
import { PhoneNumberUtil }   from 'google-libphonenumber'
import { confirmSubmission } from '../actions/data'

class ApplicantForm extends Component {
	render() {
		const { 
			fields: { firstName, lastName, region, email, phone, phoneType, over21 }, 
			handleSubmit, submitting
		} = this.props
		const hasError = field => field.touched && field.error
		return (
			<form id="application" onSubmit={handleSubmit}>
				<h2>Shopper Application</h2>
				<div>
					<span>
						<label htmlFor="first-name">First Name</label>
						{hasError(firstName) && <div className="error">{firstName.error}</div>}
					</span>
					<input id="first-name" name="first-name" type="text" {...firstName} className={hasError(firstName) && 'error'} />
				</div>
				<div>
					<span>
						<label htmlFor="last-name">Last Name</label>
						{hasError(lastName) && <div className="error">{lastName.error}</div>}
					</span>
					<input id="last-name" name="last-name" type="text" {...lastName} className={hasError(lastName) && 'error'} />
				</div>
				<div>
					<span>
						<label htmlFor="region">Region</label>
						{hasError(region) && <div className="error">{region.error}</div>}
					</span>
					<select id="region" name="region" {...region} className={hasError(region) && 'error'}>
						<option value=""></option>
						<option value="Berlin">Berlin</option>
						<option value="Boston">Boston</option>
						<option value="Chicago">Chicago</option>
						<option value="Delhi">Delhi</option>
						<option value="NYC">NYC</option>
						<option value="San Francisco Bay Area">San Francisco Bay Area</option>
						<option value="Toronto">Toronto</option>
					</select>
				</div>
				<div>
					<span>
						<label htmlFor="email">Email</label>
						{hasError(email) && <div className="error">{email.error}</div>}
					</span>
					<input id="email" name="email" type="email" {...email} className={hasError(email) && 'error'} />
				</div>
				<div>
					<span>
						<label htmlFor="phone-number">Phone Number</label>
						{hasError(phone) && <div className="error">{phone.error}</div>}
					</span>
					<input id="phone-number" name="phone-number" type="text" {...phone} className={hasError(phone) && 'error'} />
				</div>
				<div>
					<span>
						<label htmlFor="phone-type">Phone Type</label>
						{hasError(phoneType) && <div className="error">{phoneType.error}</div>}
					</span>
					<select id="phone-type" name="phone-type" {...phoneType} className={hasError(phoneType) && 'error'}>
						<option value=""></option>
						<option value="Android 4.0+ (less than 2 years old)">Android 4.0+ (less than 2 years old)</option>
						<option value="Android 2.2/2.3 (over 2 years old)">Android 2.2/2.3 (over 2 years old)</option>
						<option value="Blackberry">Blackberry</option>
						<option value="iPhone 6/6 Plus">iPhone 6/6 Plus</option>
						<option value="iPhone 6s/6s Plus">iPhone 6s/6s Plus</option>
						<option value="iPhone 5/5S">iPhone 5/5S</option>
						<option value="iPhone 4/4S">iPhone 4/4S</option>
						<option value="iPhone 3G/3GS">iPhone 3G/3GS</option>
						<option value="Windows Phone">Windows Phone</option>
						<option value="Other">Other</option>
					</select>
				</div>
				<div>
					<span className="label">Are you over 21?</span>
					<div className="checkbox-container">
						<input type="checkbox" id="over-21" name="over-21" {...over21} />
						<label className="checkbox-label" htmlFor="over-21">Yes, I am over 21 years old.</label>
					</div>
				</div>
				<div className="submit">
					<button type="submit" disabled={submitting}>Submit</button>
				</div>
			</form>
		)
	}
}

function validate(values) {
	const errors = {};

	if (!values.firstName) {
		errors.firstName = 'Required'
	}

	if (!values.lastName) {
		errors.lastName = 'Required'
	}

	if (!values.region) {
		errors.region = 'Required'
	}

	if (!values.email) {
		errors.email = 'Required'
	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		errors.email = 'Invalid email address'
	}

	if (!values.phone) {
		errors.phone = 'Required'
	}
	else {
		try {
			const phoneUtil   = PhoneNumberUtil.getInstance()
			const phoneNumber = phoneUtil.parseAndKeepRawInput(values.phone, 'US');
			const isPossible  = phoneUtil.isPossibleNumber(phoneNumber)
			if(!isPossible) {
				// Can parse as a phone number, but it is not a possible phone number
				errors.phone = 'Invalid phone number'
			}
		}
		catch (e) {
			// Cannot parse as a phone number
			errors.phone = 'Invalid phone number'
		}
	}

	if (!values.phoneType) {
		errors.phoneType = 'Required'
	}

	return errors;
}

function asyncValidate(values, dispatch) {
	return fetch('/api/shoppers/?format=json&email=' + values.email)
		.then(response => response.json())
		.then(json => {
			if(json.count > 0) {
				return Promise.reject({ email: 'That email already exists' })
			}
			else {
				return Promise.resolve()
			}
		}
	)
}

function onSubmit(values, dispatch) {
	dispatch(confirmSubmission(values))
	browserHistory.push('/shoppers/apply/confirm')
}

ApplicantForm = reduxForm({
	form: 'application',
	fields: [
		'firstName',
		'lastName',
		'region',
		'email',
		'phone',
		'phoneType',
		'over21'
	],
	asyncValidate,
	asyncBlurFields: ['email'],
	validate,
	onSubmit
})(ApplicantForm)

export default ApplicantForm