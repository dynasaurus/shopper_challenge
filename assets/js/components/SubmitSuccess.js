import React, { Component }  from 'react'
import { connect }           from 'react-redux'

class SubmitSuccess extends Component {
	render() {
		const { dispatch, values } = this.props
		const { id, email } = values
		const emailURL = encodeURIComponent(email)
		return (
			<div>
				<p>You have successfully submitted your application!</p>
				{id && <p>Please go to <a href={"/shoppers/" + emailURL}>this link</a> to view the status of your application.</p>}
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		values: state.data
	}
}

export default connect(mapStateToProps)(SubmitSuccess)
