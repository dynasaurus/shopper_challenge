import React, { Component }  from 'react'
import { connect }           from 'react-redux'
import { getApplicant }      from '../actions/data'

import { toProperCase }      from '../utils'

export default class ViewApplication extends Component {
	componentDidMount() {
		// Load the applicant data from the email URL parameter
		const { dispatch }  = this.props
		const { email }     = this.props.params
		dispatch(getApplicant(email))
	}
	render() {
		const { firstName, lastName, workflowState } = this.props.values
		return (
			<div>
				<h2>Application status for {firstName} {lastName}</h2>
				<p>{toProperCase(workflowState)}</p>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		values: state.data
	}
}

export default connect(mapStateToProps)(ViewApplication)
