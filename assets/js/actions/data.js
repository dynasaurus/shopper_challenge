import browserHistory from 'react-router/lib/browserHistory'

export const CONFIRM_SUBMISSION = 'CONFIRM_SUBMISSION'
export const ERRORS_FOUND       = 'ERRORS_FOUND'
export const UPDATE_ID          = 'UPDATE_ID'
export const LOAD_DATA          = 'LOAD_DATA'

export function confirmSubmission(values) {
	return dispatch => {
		dispatch({
			type: ERRORS_FOUND,
			errors: false,
		})
		dispatch({
			type: CONFIRM_SUBMISSION,
			values,
		})
	}
}

export function submitApplication(values) {
	return dispatch => {
		fetch('/api/shoppers/', {
			method: 'POST',
			headers: { 
				'Accept':       'application/json',
				'Content-Type': 'application/json' 
			},
			datatype: 'json',
			body: JSON.stringify(values)
		})
		.then(response => response.json())
		.then(json => {
			if(json.id) {
				dispatch({
					type: UPDATE_ID,
					id: json.id
				})
				browserHistory.push('/shoppers/apply/success')
			}
			else {
				dispatch({
					type: ERRORS_FOUND,
					errors: json,
				})
			}
		})
	}
}

export function getApplicant(email) {
	return dispatch => {
		fetch('/api/shoppers/?format=json&email=' + email)
		.then(response => response.json())
		.then(json => {
			const data  = json.results[0]
			data.over21 = !!data.over21
			dispatch({
				type: LOAD_DATA,
				values: data
			})
		})
	}
}
