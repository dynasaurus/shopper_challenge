
export function toProperCase(str) {
	// Check if str is a valid string (not empty)
	if(typeof str == 'string') {
		const noUnderscores = str.replace(/_/g, ' ')                           // Remove underscores
    	return noUnderscores.charAt(0).toUpperCase() + noUnderscores.slice(1)  // Capitalize first letter
	}
	else {
		return ''
	}
}