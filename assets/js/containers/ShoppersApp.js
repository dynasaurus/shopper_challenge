import React, { Component, PropTypes } from 'react'
import browserHistory                  from 'react-router/lib/browserHistory'

export default class ShoppersApp extends Component {
	render() {
		const { children } = this.props
		return (
			<div>
				<header>
					<h1><img src="/static/images/instacart-logo-color.png" alt="Instacart" height="32" /></h1>
				</header>
				<div id="content">
					{children}
				</div>
			</div>
		)
	}
}
