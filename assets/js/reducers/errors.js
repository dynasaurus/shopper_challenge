import { ERRORS_FOUND } from '../actions/data'

export default function errors(state = {}, action) {
	switch (action.type) {
		case ERRORS_FOUND:
			return action.errors
		default:
			return state
	}
}
