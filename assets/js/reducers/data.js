import { CONFIRM_SUBMISSION, LOAD_DATA, UPDATE_ID } from '../actions/data'

const defaultState = {
	id:            null,
	email:         '',
	firstName:     '',
	lastName:      '',
	over21:        false,
	phone:         '',
	phoneType:     '',
	region:        '',
	workflowState: null
}

export default function data(state = defaultState, action) {
	switch (action.type) {
		case CONFIRM_SUBMISSION:
		case LOAD_DATA:
			return action.values
		case UPDATE_ID:
			// Shallow copy state
			const newState = Object.assign({}, state)
			newState.id = action.id
			return newState
		default:
			return state
	}
}
