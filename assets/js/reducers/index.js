import { combineReducers } from 'redux'
import reducer             from 'redux-form/lib/reducer'
import { routerReducer }   from 'react-router-redux'
import data   from './data'
import errors from './errors'

const reducers = combineReducers({
	data:    data,
	errors:  errors,
	routing: routerReducer,
	form:    reducer
})

export default reducers
