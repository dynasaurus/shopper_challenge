import React, { combineReducers } from 'react'
import { render }                 from 'react-dom'
import { Provider }               from 'react-redux'

import Router               from 'react-router/lib/Router'
import Route                from 'react-router/lib/Route'
import IndexRoute           from 'react-router/lib/IndexRoute'
import browserHistory       from 'react-router/lib/browserHistory'
import syncHistoryWithStore from 'react-router-redux/lib/sync'

import ShoppersApp       from './containers/ShoppersApp'
import LandingPage       from './components/LandingPage'
import ApplicantForm     from './components/ApplicantForm'
import ConfirmSubmission from './components/ConfirmSubmission'
import SubmitSuccess     from './components/SubmitSuccess'
import ViewApplication   from './components/ViewApplication'
import configureStore    from './store/configureStore'

const store = configureStore()

const history = syncHistoryWithStore(browserHistory, store)

render(
	<Provider store={store}>
		<Router history={history}>
			<Route path="/shoppers" component={ShoppersApp}>
          		<IndexRoute component={LandingPage}/>
				<Route path="/shoppers/apply" component={ApplicantForm} />
				<Route path="/shoppers/apply/confirm" component={ConfirmSubmission} />
				<Route path="/shoppers/apply/success" component={SubmitSuccess} />
				<Route path="/shoppers/:email" component={ViewApplication} />
			</Route>
		</Router>
	</Provider>,
	document.getElementById('shoppers-app')
)
