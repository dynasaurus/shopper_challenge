# Instacart Shopper Challenge

This is a simple form for users to apply as Instacart Shoppers. The user will be greeted by a landing page, from where they can enter their information into a form to apply to be an Instacart Shopper. Their information is then entered into the database. There is an API at funnels.json, where a program can give a start_date (and optional end_date) to view a JSON list of the count of how many applicants at each stage of the process, grouped by week.

## Installation

### Requirements

1. python
2. pip
3. npm

### Django

1. Install pip packages from requirements.txt
```bash
pip install -r requirements.txt
```

2. Install npm packages
```bash
npm install
```

3. Copy development.sqlite3 in the root directory

### Run development
Run the server and open http://localhost:8000 in your browser
```bash
npm run serve
```
This command will start both the Django server and the webpack builder

### Run production
1. Build the minified webpack bundle
```bash
npm run build
```

2. Run the server and open http://localhost:8000 in your browser
```bash
python manage.py runserver
```

## Design Decisions

I chose ReactJS as the main JavaScript library because I have experience with it, it has relatively good support, and it allows the creation of good user experiences as opposed to a static website. There are some downsides to this because ReactJS is relatively new and with all the ECMAScript changes, sometimes the libraries I want to use are not compatible with the version of ES I am using. It also requires a JavaScript (webpack) transpiler because browsers may not support the syntax.

I chose to use Django for the backend also because I have experience with it, it pairs decently with ReactJS, and it also has fairly good support. It also helps keep everything organized and is fairly intuitive to use, although it sometimes can be a bit more work to set everything up than some other backend choices.

I chose to use pure CSS because this project is small enough that a CSS pre-processor would not be that helpful.

One downside is that there is not a great way to render the React components server side. Search engine indexers and users who may have disabled JavaScript will not be able to access the page. This could be a future improvement by finding a way to pre-render the component server-side on page request and allow Django to serve that HTML.

## Future possibilities
Ways to improve the application, given more time:
1. Modify the database with an indexed week number column to improve performance of funnels.json
2. Use a not-easy to guess unique ID for the link that applicant can view their status at (like a unique hash). Right now, someone could pretty easily look at another person's application status just by knowing their email address.
3. Create a UI to update the application status
4. Write tests for error checking and the API
5. Pre-render the JavaScript server-side
6. Store the state in the session so the user does not lose their form data if they refresh the page accidentally.
7. Validate phone number server-side (The library to do this [django-phonenumber-field] will also change format the phone number by default and requires a country code)

## Attribution

- arrow-down.png source: https://www.iconfinder.com/icons/352466/arrow_down_keyboard_icon
- checkmark.png source:  https://www.iconfinder.com/icons/103184/check_checkmark_ok_yes_icon